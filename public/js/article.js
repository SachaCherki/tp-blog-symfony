$('#add-image').click(function(){
	console.log('ok-2');
	const index = +$('#widgets-counter').val();

	console.log(index);

	const tmpl = $('#article_images').data('prototype').replace(/__name__/g, index);

	$("#article_images").append(tmpl);

	$("#widgets-counter").val(index + 1)

	handleDeleteButtons()
})

function handleDeleteButtons(){
	console.log('ok');
	// Au click sur le bouton avec l'attribut data-action="delete"
	$('button[data-action="delete"]').click(function(){

		// On récupèère la cible contenu dans l'attribut data-target
		const target = this.dataset.target;

		// On supprime la cible
		$(target).remove();

	})
}

function updateCounter(){
	const count = +$('#article_images div.form-group').length;

	$('#widgets-counter').val(count);
}

updateCounter();

handleDeleteButtons();