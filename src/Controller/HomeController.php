<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Service\Utilities;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

	/**
	 * @Route("/", name="homepage")
	 */
	public function home(ArticleRepository $repo)
	{
		$limit = 3;

		$articles = $repo->findBy([], ['id' => 'DESC'], $limit);

		$total = count($repo->findAll());

		return $this->render('home.html.twig', [
			'articles' => $articles,
			'total' => $total
		]);
	}
}