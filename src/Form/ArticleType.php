<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Author;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
	public function getConfiguration($label, $placeholder, $options = []){
		return array_merge([
			'label' => $label,
			'attr' => [
				'placeholder' => $placeholder
			]
		], $options);
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,$this->getConfiguration('Titre', 'Tapez un titre pour l\'article'))
            ->add('content',  TextareaType::class, $this->getConfiguration('Description détaillée', 'Tapez une description qui donne envie de livre votre article'))
            ->add('slug', TextType::class, $this->getConfiguration('Chaîne url', 'Adresse web (automatique)', ['required' => false]))
            ->add('coverImage', UrlType::class, $this->getConfiguration('URL de l\'image principale', 'Donnez l\'adresse de l\'image'))
			->add(
				'images',
				CollectionType::class,
				[
					'entry_type' => ImageType::class,
					'allow_add' => true,
					'allow_delete' => true
				]
			)->add('author', EntityType::class, [
				'class' => Author::class,
				'choice_label' => function($author) {
					return $author->getDisplayName();
				},
				'label' => 'Auteur'
			]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
