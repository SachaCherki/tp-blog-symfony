<?php

namespace App\Service;

class Pagination{
	public function pagination($repo, $nbArticle, $page){
		$limit = $nbArticle;

		$start = $page * $limit - $limit;

		$total = count($repo->findAll());

		$pages = ceil($total / $limit);

		return array(
			"articles" => $repo->findBy([], [], $limit, $start),
			"pages" => $pages
		);
	}
}