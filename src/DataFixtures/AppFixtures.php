<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	$faker = Factory::create("fr_FR");
		// $slugify = new Slugify();

		for($k = 1; $k <= 5; $k++){
			$author = new Author();

			$author->setName($faker->lastName());
			$author->setFirstname($faker->firstName());

			for ($i = 1; $i <= 10; $i++){
				$article = new Article();

				$article->setAuthor($author);
				$titre = $faker->sentence();
				// $slug = $slugify->slugify($titre);
				$coverImage = $faker->imageUrl();
				$content = "<p>" . join('</p><p>', $faker->paragraphs(5)) . "</p>";

				$article->setTitle($titre)
					->setCoverImage($coverImage)
					->setContent($content);

				for ($j = 0; $j <= mt_rand(2, 5); $j++){
					$image = new Image();

					$image->setUrl($faker->imageUrl())
						->setCaption($faker->sentence())
						->setArticle($article);

					$manager->persist($image);
				}

				$manager->persist($article);
			}

			$manager->persist($author);

		}
		$manager->flush();
    }
}
